export class Model {
  id: number;
  created: Date;
  updated: Date;

  constructor(fields?: any) {
    this.id = fields && fields.id || 0;
    this.created = fields && fields.created_at && new Date(fields.created_at.replace(/-/g, '/')) || null;
    this.updated = fields && fields.updated_at && new Date(fields.updated_at.replace(/-/g, '/')) || null;
  }
}
