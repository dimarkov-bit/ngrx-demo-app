import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {Account} from '../../accounts/models/account';

const httpHeaders = new HttpHeaders({
  'Authorization': 'Basic  ' + btoa('dimarkov@outlook.com:burunduk2050')
});

@Injectable({
  providedIn: 'root',
})
export class AccountsRepositoryService {
  private API_PATH = 'https://dev.dimarkov.net/api/our-cash-flows' + '/accounts';

  constructor(private http: HttpClient) {
  }

  searchAccounts(queryTitle: string): Observable<Account[]> {
    return this.http
      .get<{ items: Account[] }>(`${this.API_PATH}`, {
        headers: httpHeaders
      })
      .pipe(map(books => books.items || []));
  }

  retrieveAccount(volumeId: string): Observable<Account> {
    return this.http.get<Account>(`${this.API_PATH}/${volumeId}`);
  }
}
