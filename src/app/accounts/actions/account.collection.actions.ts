import { Action } from '@ngrx/store';
import { Account } from '../models/account';

export enum CollectionActionTypes {
  AddAccount = '[Collection] Add Account',
  AddAccountSuccess = '[Collection] Add Account Success',
  AddAccountFail = '[Collection] Add Account Fail',
  RemoveAccount = '[Collection] Remove Account',
  RemoveAccountSuccess = '[Collection] Remove Account Success',
  RemoveAccountFail = '[Collection] Remove Account Fail',
  Load = '[Collection] Load',
  LoadSuccess = '[Collection] Load Success',
  LoadFail = '[Collection] Load Fail',
}

/**
 * Add Account to Collection Actions
 */
export class AddAccount implements Action {
  readonly type = CollectionActionTypes.AddAccount;

  constructor(public payload: Account) {}
}

export class AddAccountSuccess implements Action {
  readonly type = CollectionActionTypes.AddAccountSuccess;

  constructor(public payload: Account) {}
}

export class AddAccountFail implements Action {
  readonly type = CollectionActionTypes.AddAccountFail;

  constructor(public payload: Account) {}
}

/**
 * Remove Account from Collection Actions
 */
export class RemoveAccount implements Action {
  readonly type = CollectionActionTypes.RemoveAccount;

  constructor(public payload: Account) {}
}

export class RemoveAccountSuccess implements Action {
  readonly type = CollectionActionTypes.RemoveAccountSuccess;

  constructor(public payload: Account) {}
}

export class RemoveAccountFail implements Action {
  readonly type = CollectionActionTypes.RemoveAccountFail;

  constructor(public payload: Account) {}
}

/**
 * Load Collection Actions
 */
export class Load implements Action {
  readonly type = CollectionActionTypes.Load;
}

export class LoadSuccess implements Action {
  readonly type = CollectionActionTypes.LoadSuccess;

  constructor(public payload: Account[]) {}
}

export class LoadFail implements Action {
  readonly type = CollectionActionTypes.LoadFail;

  constructor(public payload: any) {}
}

export type CollectionActionsUnion =
  | AddAccount
  | AddAccountSuccess
  | AddAccountFail
  | RemoveAccount
  | RemoveAccountSuccess
  | RemoveAccountFail
  | Load
  | LoadSuccess
  | LoadFail;
