import {Action} from '@ngrx/store';
import {Account} from '../models/account';

export enum AccountActionTypes {
  Search = '[Account] Search',
  SearchComplete = '[Account] Search Complete',
  SearchError = '[Account] Search Error',
  Load = '[Account] Load',
  Select = '[Account] Select'
}

export class Search implements Action {
  readonly type = AccountActionTypes.Search;

  constructor(public payload: string) {
  }
}

export class SearchComplete implements Action {
  readonly type = AccountActionTypes.SearchComplete;

  constructor(public payload: Account[]) {
  }
}

export class SearchError implements Action {
  readonly type = AccountActionTypes.SearchError;

  constructor(public payload: string) {
  }
}

export class Load implements Action {
  readonly type = AccountActionTypes.Load;

  constructor(public payload: Account) {
  }
}

export class Select implements Action {
  readonly type = AccountActionTypes.Select;

  constructor(public payload: string) {
  }
}

export type AccountActionsUnion =
  | Search
  | SearchComplete
  | SearchError
  | Load
  | Select;
