import {Model} from '../../core/entities/model';

export class Account extends Model {
  name: string;
  type: string;
  userId: number;
  groupId: number;

  balance: number;

  constructor(fields?: any) {
    super(fields);

    this.name = fields && fields.name || '';
    this.type = fields && fields.type || '';
    this.userId = fields && fields.userId || null;
    this.groupId = fields && fields.groupId || null;

    this.balance = fields && fields.balance || 0;
  }

}
