import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Account } from '../models/account';
import { AccountActionsUnion, AccountActionTypes } from '../actions/acccount.actions';
import {
  CollectionActionsUnion,
  CollectionActionTypes,
} from '../actions/account.collection.actions';

export interface State extends EntityState<Account> {
  selectedAccountId: string | null;
}

export const adapter: EntityAdapter<Account> = createEntityAdapter<Account>({
  selectId: (account: Account) => account.id,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
  selectedAccountId: null,
});

export function reducer(
  state = initialState,
  action: AccountActionsUnion | CollectionActionsUnion
): State {
  switch (action.type) {
    case AccountActionTypes.SearchComplete:
    case CollectionActionTypes.LoadSuccess: {
      /**
       * The addMany function provided by the created adapter
       * adds many records to the entity dictionary
       * and returns a new state including those records. If
       * the collection is to be sorted, the adapter will
       * sort each record upon entry into the sorted array.
       */
      return adapter.addMany(action.payload, state);
    }

    case AccountActionTypes.Load: {
      /**
       * The addOne function provided by the created adapter
       * adds one record to the entity dictionary
       * and returns a new state including that records if it doesn't
       * exist already. If the collection is to be sorted, the adapter will
       * insert the new record into the sorted array.
       */
      return adapter.addOne(action.payload, state);
    }

    case AccountActionTypes.Select: {
      return {
        ...state,
        selectedAccountId: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */

export const getSelectedId = (state: State) => state.selectedAccountId;
