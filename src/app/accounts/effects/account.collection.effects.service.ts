import {Injectable} from '@angular/core';
import {Database} from '@ngrx/db';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {defer, Observable, of} from 'rxjs';
import {catchError, map, mergeMap, switchMap, toArray} from 'rxjs/operators';

import {Account} from '../models/account';
import {
  AddAccount,
  AddAccountFail,
  AddAccountSuccess,
  CollectionActionTypes,
  LoadFail,
  LoadSuccess,
  RemoveAccount,
  RemoveAccountFail,
  RemoveAccountSuccess,
} from '../actions/account.collection.actions';

@Injectable()
export class AccountCollectionEffectsService {
  @Effect({dispatch: false})
  openDB$: Observable<any> = defer(() => {
    return this.db.open('accounts_app');
  });

  @Effect()
  loadCollection$: Observable<Action> = this.actions$.pipe(
    ofType(CollectionActionTypes.Load),
    switchMap(() =>
      this.db.query('accounts').pipe(
        toArray(),
        map((accounts: Account[]) => new LoadSuccess(accounts)),
        catchError(error => of(new LoadFail(error)))
      )
    )
  );

  @Effect()
  addAccountToCollection$: Observable<Action> = this.actions$.pipe(
    ofType<AddAccount>(CollectionActionTypes.AddAccount),
    map(action => action.payload),
    mergeMap(account =>
      this.db.insert('accounts', [account]).pipe(
        map(() => new AddAccountSuccess(account)),
        catchError(() => of(new AddAccountFail(account)))
      )
    )
  );

  @Effect()
  removeAccountFromCollection$: Observable<Action> = this.actions$.pipe(
    ofType<RemoveAccount>(CollectionActionTypes.RemoveAccount),
    map(action => action.payload),
    mergeMap(account =>
      this.db.executeWrite('accounts', 'delete', [account.id]).pipe(
        map(() => new RemoveAccountSuccess(account)),
        catchError(() => of(new RemoveAccountFail(account)))
      )
    )
  );

  constructor(private actions$: Actions, private db: Database) {
  }
}
