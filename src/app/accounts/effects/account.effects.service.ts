import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {asyncScheduler, EMPTY, Observable, of} from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  skip,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import {
  AccountActionTypes,
  Search,
  SearchComplete,
  SearchError,
} from '../actions/acccount.actions';
import {Account} from '../models/account';
import {AccountsRepositoryService} from '../../core/services/accounts.repository.service';

@Injectable()
export class AccountEffects {
  @Effect()
  search$ = ({debounce = 300, scheduler = asyncScheduler} = {}): Observable<Action> =>
    this.actions$.pipe(
      ofType<Search>(AccountActionTypes.Search),
      debounceTime(debounce, scheduler),
      map(action => action.payload),
      switchMap(query => {
        if (query === '') {
          return EMPTY;
        }

        const nextSearch$ = this.actions$.pipe(
          ofType(AccountActionTypes.Search),
          skip(1)
        );

        return this.accountsRepository.searchAccounts(query).pipe(
          takeUntil(nextSearch$),
          map((books: Account[]) => new SearchComplete(books)),
          catchError(err => of(new SearchError(err)))
        );
      })
    );

  constructor(
    private actions$: Actions,
    private accountsRepository: AccountsRepositoryService
  ) {
  }
}
